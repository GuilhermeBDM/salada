﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SillasPring
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string texto;
        int espaco;
        int vogal;

        char[] vogais = { 'a', 'e', 'i', 'o', 'u' };

        private string inverso(string texto)
        {
            char[] arrChar = texto.ToCharArray();
            Array.Reverse(arrChar);
            string invertida = new String(arrChar);

            return invertida;
        }
        
        private void txtTexto_TextChanged(object sender, EventArgs e)
        {

            txtInvertido.Text = inverso(txtTexto.Text);

            texto = txtTexto.Text; 

            while (texto.Contains(' '))
            {
                espaco = texto.IndexOf(' ');
                texto = texto.Remove(espaco, 1);
            }

            lblLetras.Text = texto.Length.ToString();

            int qtdVogal = 0;

            foreach (var item in vogais)
            { 
                while (texto.Contains(item))
                {
                    vogal = texto.IndexOf(item);
                    texto = texto.Remove(vogal, 1);
                    qtdVogal++;
                }

                lblVogais.Text = qtdVogal.ToString();
            }

            lblConsoantes.Text = texto.Length.ToString();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtTexto.Text = string.Empty;
        }

        private void txtInvertido_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPalavrasInvertidas_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
